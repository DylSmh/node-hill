const regexMatch = /([^"]+)(?:\"([^\"]+)\"+)?/

function getUserAndReason(msg = '') {
    let match = msg.match(regexMatch)
    if (!match) return

    let user = match[1] && match[1].trim()
    if (!user) return

    let reason = match[2]

    // Check for an EXACT username match
    for (let p of Game.players) {
        let victim = p.username.toLowerCase()
        if (victim === user) {
            return [p, reason]
        }
    }

    // Check for a partial match
    for (let p of Game.players) {
        let victim = p.username.toLowerCase()
        if (victim.startsWith(user)) {
            return [p, reason]
        }
    }

    return [null, null]
}

Game.command("kick", (p, msg) => {
    if (!msg) return
    if (!p.admin) return

    msg = msg.toLowerCase().trim()

    const [victim, reason] = getUserAndReason(msg)

    if (victim) {
        return victim.kick(reason)
    } else {
        p.message("User not found.")
    }
})